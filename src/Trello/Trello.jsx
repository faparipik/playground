import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import Axios from "axios";
import { key, token } from "../config/trelloKeys";
import "./trello.css";

class Trello extends Component {
  state = {
    data: []
  };

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const mvpList = await Axios(
      `https://api.trello.com/1/lists/5d88bdf72391de1dc0ffa8a5/cards?fields=name%2Cclosed%2CidBoard%2Cpos&key=${key}&token=${token}`
    );
    const scoreList = await Axios(
      `https://api.trello.com/1/lists/5d7b8395e51c8d238d5ab4b1/cards?fields=name%2Cclosed%2CidBoard%2Cpos&key=${key}&token=${token}`
    );
    const data = scoreList.data.map(scorer => {
      const nameSplit = scorer.name.split("-");
      let isMvp = false;
      mvpList.data.forEach(mvp => {
        const mvpNameSplit = mvp.name.split("-");
        if (
          mvpNameSplit[1].replace(/ /g, "") === nameSplit[0].replace(/ /g, "")
        ) {
          isMvp = true;
        }
        return {
          mvpName: mvpNameSplit[1].replace(/ /g, "")
        };
      });
      return {
        id: scorer.id,
        name: nameSplit[0].replace(/ /g, ""),
        goals: +nameSplit[1],
        gamesPlayed: +nameSplit[2],
        averageGoals: Number(nameSplit[1] / Number(nameSplit[2])).toFixed(2),
        isMvp
      };
    });
    this.setState({ data });
  }

  render() {
    return (
      <>
        <Row>
          {this.state.data.map(i => {
            let mvp = i.isMvp ? "MVP" : "";
            let averageGoals = isNaN(i.averageGoals) ? 0 : i.averageGoals;
            return (
              <Col
                xs={3}
                className="card"
                // style={{
                //   marginLeft: "4%",
                //   marginTop: "1.2%",
                //   border: "solid 1px black"
                // }}
              >
                <div key={i.id}>
                  <Row className="cardHeader">
                    <Col xs={9}>
                      <h3 className="textCenter">{i.name}</h3>
                    </Col>
                    <Col xs={3}>
                      <h5 className="text-right">{mvp}</h5>
                    </Col>
                  </Row>
                  <h5>{`Goals: ${i.goals}`}</h5>
                  <h5>{`Games Played: ${i.gamesPlayed}`}</h5>
                  <h5>{`Average Goals: ${averageGoals}`}</h5>
                </div>
              </Col>
            );
          })}
        </Row>
      </>
    );
  }
}

export default Trello;
